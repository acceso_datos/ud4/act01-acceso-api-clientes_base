package service;

import java.util.List;

import modelo.Cliente;
import repository.ClienteRepository;
import vista.Utilidades;

/* Ejemplo de capa service del dominio del cliente (usuario). 
 * Requiere modificar la tabla clientes añadiendo
 * los campos username y password
 * alter table clientes
		add column username varchar(20) unique,
		add column password char(40);

    update clientes 
		set username = concat("usr", id),
			password = sha1('1234');
 */
public class ClienteService {
	ClienteRepository clienteRepository;

	public ClienteService() throws Exception {
		clienteRepository = new ClienteRepository();
	}

	// Comprobar si ya existe (username) con findByUserName
	// Si existe: abortar (exception)
	// Sino, realiza inserción
	public void registraCliente(ClienteBasicDTO cli) throws Exception {
		if (!clienteRepository.existsByUsername(cli.getUsername())) {
			cli.setPassword(Utilidades.sha1(cli.getPassword()));
			if (clienteRepository.save(cli) == null) {
				throw new Exception("Error al registrar usuario");
			}
		} else {
			throw new Exception("El username ya está cogido");
		}

	}

	// Comprobar si existe (username) con findByUserName
	// Si existe, comprueba password.
	// Sino, abortar (exception)
	// También se podría añadir: si contraseña caducada, abortar (excepcion).
	public Cliente loginCliente(String username, String password) throws Exception {
		Cliente cliente = clienteRepository.getByUsername(username);
		if (cliente != null) {
			String pw = Utilidades.sha1(password);
			if (!pw.equals(cliente.getPassword())) {
				throw new Exception("Fallo de autenticación");
			}
		} else {
			throw new Exception("Fallo de autenticación");
		}
		return cliente;
	};

	// Eliminar cliente 
	public void bajaCliente(Cliente cli) throws Exception {
		if (clienteRepository.delete(cli) == false) {
			throw new Exception("Imposible dar de baja el cliente");
		}

	}

	// Cambio de la contraseña del cliente (id)
	public void cambiaContrasenya(int idCliente, String password) throws Exception {
		Cliente cliente = clienteRepository.get(idCliente);
		if (cliente != null) {
			cliente.setPassword(Utilidades.sha1(password));
			clienteRepository.save(cliente);
		} else {
			throw new Exception("El cliente especificado no existe");
		}
	}

	// Se usa para cambiar los datos del perfil del usuario: nombre, direccion,...
	public void cambiaPerfil(Cliente cli) throws Exception {
		if (cli.getId() > 0 && clienteRepository.exists(cli.getId())) {
			clienteRepository.save(cli);
		} else {
			throw new Exception("Ha habido un problema actualizando el perfil");
		}
	}

	// Busca clientes por aproximación de nombre
	public List<Cliente> buscaPorNombre(String nombre) throws Exception {
		List<Cliente> listaResultado = clienteRepository.getByName(nombre);
		if (listaResultado.size() == 0) {
			throw new Exception("No se han encontrado clientes con ese nombre");
		} else {
			return listaResultado;
		}
	}

	// Obtención de un cliente
	public Cliente obtenerCliente(int idCliente) throws Exception {
		Cliente cli = clienteRepository.get(idCliente);
		if (cli != null) {
			return cli;
		} else {
			throw new Exception("Ha habido un problema obteniendo cliente. Probablemente no existe");
		}
	}
	
	// Obtención de facturas del cliente
	public List<Integer> obtenFacturas(int clienteId) throws Exception {
		return clienteRepository.getFacturas(clienteId);
	}

	// Obtención total facturado por el cliente
	public float calculaTotalFacturado(int clienteId) throws Exception {
		return clienteRepository.calculaTotalFacturado(clienteId);
	}
	
	// Obtención última factura del cliente
	public Factura obtenUltimaFactura(int clienteId) throws Exception {
		return clienteRepository.getUltimaFactura(clienteId);
	}

}
