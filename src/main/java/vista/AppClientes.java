package vista;

import java.util.List;

import modelo.Cliente;
import modelo.Factura;
import service.ClienteService;

public class AppClientes {
	static ClienteService clienteServicio = null;

	public static void main(String[] args) {
		casosUsoClientes();	
	}

	private static void casosUsoClientes() {
		try {
			clienteServicio = new ClienteService();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

		if (clienteServicio != null) {
			ejemploLogin();
			ejemploCambioContrasenya();
			ejemploCambioPerfil();
			ejemploRegistro();
			ejemploBusquedaPorNombre();
			ejemploMostrarTotalFacturacion();
			ejemploListarFacturas();
			ejemploVerUltimaFactura();
		}		
	}

	private static void ejemploLogin() {
		try {
			Cliente cli = clienteServicio.loginCliente("usr12", "1234");
			System.out.println("Autenticado " + cli);
		} catch (Exception e) {
			System.out.println("Error en autenticación: " + e.getMessage());
		}

	}

	private static void ejemploCambioContrasenya() {
		try {
			clienteServicio.cambiaContrasenya(12, "1234");
			System.out.println("Contraseña cambiada con éxito (cliente 12)");
		} catch (Exception e) {
			System.out.println("Error cambiando contraseña: " + e.getMessage());
		}

	}

	private static void ejemploCambioPerfil() {
		try {
			Cliente cli = clienteServicio.obtenerCliente(12);
			cli.setNombre("Mateo");
			clienteServicio.cambiaPerfil(cli);
			System.out.println("Perfil cambiado satisfactoriamente: " + cli);
		} catch (Exception e) {
			System.out.println("Error cambiando perfil: " + e.getMessage());
		}

	}

	private static void ejemploRegistro() {
		try {
			Cliente nuevoCliente = new Cliente("Aa Aaaa", "C/ Aa", "usr_e", "1234");
			clienteServicio.registraCliente(nuevoCliente);
			System.out.println("Registro nuevo usuario OK: " + nuevoCliente);
		} catch (Exception e) {
			System.out.println("Error registrando usuario: " + e.getMessage());
		}

	}

	private static void ejemploBusquedaPorNombre() {
		try {
			System.out.println("Busqueda por nombre 'm'");
			List<Cliente> listaClientes = clienteServicio.buscaPorNombre("m");
			for (Cliente c : listaClientes) {
				System.out.println(c);
			}
		} catch (Exception e) {
			System.out.println("Error en búsqueda: " + e.getMessage());
		}

	}
	
	private static void ejemploMostrarTotalFacturacion() {		
		int clienteId = 1;		
		float total;
		try {
			System.out.print("El total facturado del cliente " + clienteId + " es: ");
			total = clienteServicio.calculaTotalFacturado(clienteId);
			System.out.println(total+" €");
		} catch (Exception e) {
			System.out.println("Error calculando total facturacion: " + e.getMessage());
		}	
		
	}
	
	private static void ejemploListarFacturas() {		
		int clienteId = 1;		
		try {
			List<Integer> listaFacturas = clienteServicio.obtenFacturas(clienteId);
			// mostramos solo las 100 primeras
			for (int i = 0; i < 100; i++) {
				System.out.print(listaFacturas.get(i) + " ");
			}
			
		} catch (Exception e) {
			System.out.println("Error leyendo facturas: " + e.getMessage());
		}			
	}
	
	private static void ejemploVerUltimaFactura() {
		int clienteId = 1;		
		try {
			Factura factura = clienteServicio.obtenUltimaFactura(clienteId);
			System.out.println("La última factura del cliente " + clienteId + " es: ");
			System.out.println(factura);
			
		} catch (Exception e) {
			System.out.println("Error leyendo factura: " + e.getMessage());
		}			
		
	}

}
