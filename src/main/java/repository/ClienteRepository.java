package repository;

import java.util.ArrayList;
import java.util.List;

import dao.ClienteDAO;
import modelo.Cliente;
import modelo.Factura;

public class ClienteRepository {
	ClienteDAO clienteDao;

	public ClienteRepository() throws Exception {
		clienteDao = new ClienteDAO();
	}

	public Cliente get(int id) throws Exception {
		return clienteDao.find(id);
	}

	public Cliente getByUsername(String userName) throws Exception {
		return null;
	}

	public boolean exists(int id) throws Exception {
		return false;
	}

	public boolean existsByUsername(String userName) throws Exception {
		return false;
	}

	public Cliente save(Cliente cliente) throws Exception {
		return null;
	}

	public boolean delete(Cliente cliente) throws Exception {
		return clienteDao.delete(cliente);
	}

	public List<Cliente> getByName(String nombre) throws Exception {
		return new ArrayList<Cliente>();
	}

	public float calculaTotalFacturado(int clienteId) throws Exception {
		return 0;
	}
	
	public List<Integer> getFacturas(int clienteId) throws Exception {
		return new ArrayList<Integer>();
	}
	
	public Factura getUltimaFactura(int clienteId) throws Exception {
		return null;
	}

}
